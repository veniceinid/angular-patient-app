import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PatientSearchComponent} from './patient-search/patient-search.component';
import {PatientDetailComponent} from './patient-detail/patient-detail.component';
import {PatientAddComponent} from './patient-add/patient-add.component';

const routes: Routes = [
  {
    path: '',
    component: PatientSearchComponent,
    pathMatch: 'full'
  },
  {
    path: 'patient/:id',
    component: PatientDetailComponent,
    pathMatch: 'full'
  },
  {
    path: 'add',
    component: PatientAddComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
